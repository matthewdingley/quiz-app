/**
 * Module that loads quiz data: pretend it's from somewhere more complicated
 * than a JSON file.
 */
const fs = require('fs-extra');
const { ID_REGEXP } = require('./validation');

/**
 * Returns the list of quizzes based on the current JSON file.
 */
function listQuizzes() {
  return fs.readJson('./data/quizzes.json');
}

/**
 * Returns the set of questions for one quiz based on the matching JSON file.
 */
function listQuizQuestions(quizId) {
  // Would assume this would be done anyway via a sensible e.g. database DAO etc
  // but as we're accessing the file system, just to be sure, check the quiz ID.
  if (!ID_REGEXP.test(quizId)) {
    throw new Error('Invalid Quiz ID');
  }
  return fs.readJson(`./data/quiz${quizId}.json`)
    .then(({ questions }) => questions);
}

module.exports = { listQuizzes, listQuizQuestions };
