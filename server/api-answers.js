/**
 * Module holding API endpoints for submitting and fetching user's answers.
 * Note that this takes the stance that the actual correct answer should never
 * be sent back to the client in order to avoid leaking answers.
 */

const { listQuizQuestions } = require('./quiz-data');

// TODO(matt): This has saved the actual answers - lots of points to decide
// here regarding the correct data to save based on usage.
// User answers are keyed by user ID where each user ID is mapped to a further
// object keyed by quiz ID. This allows looking up all answers by user - looking up
// answers by quiz would however require iterating through all users which would
// certainly be avoided with a real database with indices.
const userAnswers = new Map();

// TODO(matt): When the data is being stored via a proper database, it'd be best to
// refactor the code here into different modules.

/**
 * Marks the provided answers and returns which ones were correct.
 * @param {String} quizId The ID of the quiz to mark against.
 * @param {Object} answers An object mapping from question ID to the ID of the
 *     user's answer.
 * @returns {Array.<String>} The IDs of the questions that were answered correctly.
 */
function markAnswers(quizId, answers) {
  return listQuizQuestions(quizId)
    .then((currentQuestions) => {
      const correctlyAnsweredQuestionIds = [];
      currentQuestions.forEach((question) => {
        const { id, correctAnswer } = question;
        const givenAnswer = answers[id];
        if (correctAnswer === givenAnswer) {
          correctlyAnsweredQuestionIds.push(id);
        }
      });
      return correctlyAnsweredQuestionIds;
    });
}

/**
 * API endpoint to receive a user's answers, save them and return the results.
 * @param {Object} req The submitted request.
 * @returns {Promise} Returns a promise that resolves with an array of the IDs of
 *     the questions that were answered correctly.
 */
function submitUserAnswers(req) {
  // TODO(matt): Only allow submission of answers for the currently authenticated user
  const { userId, quizId } = req.params;  // userId, quizId validated by URI regex
  const { answers } = req.body;

  let thisUsersAnswers = userAnswers.get(userId);
  if (thisUsersAnswers === undefined) {
    thisUsersAnswers = new Map();
    userAnswers.set(userId, thisUsersAnswers);
  }
  thisUsersAnswers.set(quizId, answers);

  return markAnswers(quizId, answers)
    .then((correctlyAnsweredQuestionIds) => {
      return { correctAnswers: correctlyAnsweredQuestionIds };
    });
}

/**
 * API endpoint to fetch a user's previous answers to a quiz.
 * This will set the response object to return a 404 status in the event of
 * the user ID or quiz ID being unrecognised.
 *
 * @param {Object} req The submitted request.
 * @param {Object} resp The response object.
 * @returns {Promise} A promise that will resolve with the IDs of the questions
 *     the user previously answered correctly if the user and quiz ID are recognised.
 */
function getUserAnswers(req, resp) {
  // TODO(matt): Only allow request of answers for the currently authenticated user
  const { userId, quizId } = req.params;  // userId, quizId validated by URI regex
  let thisUsersAnswers = userAnswers.get(userId);
  if (thisUsersAnswers === undefined) {
    resp.sendStatus(404);
    return Promise.resolve();
  }
  const thisQuizAnswers = thisUsersAnswers.get(quizId);
  if (thisQuizAnswers === undefined) {
    resp.sendStatus(404);
    return Promise.resolve();
  }
  return markAnswers(quizId, thisQuizAnswers)
    .then((correctlyAnsweredQuestionIds) => {
      return { correctAnswers: correctlyAnsweredQuestionIds };
    });
}

module.exports = { submitUserAnswers, getUserAnswers };
