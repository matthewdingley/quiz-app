const ID_REGEXP_STR = '[a-zA-Z0-9_]+';
const ID_REGEXP = new RegExp(`^${ID_REGEXP_STR}$`);

module.exports = { ID_REGEXP_STR, ID_REGEXP };
