const express = require('express');
const apiQuiz = require('./api-quiz');
const apiAnswers = require('./api-answers');
const { ID_REGEXP_STR } = require('./validation');

const PORT = 5000;

const app = express();

/**
 * A helper function to respond to a request with controller.
 * This produces a callback function that will delegate each request to the
 * provided controller. The controller should return a Promise that resolves to
 * an object value. That object will be sent returned as a JSON encoded
 * response to the client. Any errors are passed straight onto the express
 * framework.
 */
function produceCallbackFromController(controller) {
  return (req, res, next) => controller(req)
    .then(response => res.json(response))
    // TODO(matt): Implement a more rigorous error handling strategy at the
    // Express level
    // TODO(matt): Consider a better way to set the status code in the case of
    // an error - currently set from within the controller
    .catch(next);
}

app.use(express.json());


// TODO(matt): Install helmet

app.get('/api/quizzes/', produceCallbackFromController(apiQuiz.getAllQuizzes));
app.post(
  `/api/user/:userId(${ID_REGEXP_STR})/answers/:quizId(${ID_REGEXP_STR})`, produceCallbackFromController(apiAnswers.submitUserAnswers));
app.get(
  `/api/quiz/:quizId(${ID_REGEXP_STR})/questions`, produceCallbackFromController(apiQuiz.getQuizQuestions));
app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`)); // eslint-disable-line no-console