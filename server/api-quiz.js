const { listQuizzes, listQuizQuestions } = require('./quiz-data');

function getAllQuizzes() {
  return listQuizzes()
    .then(quizzes => ({ quizzes }));
}

function getQuizQuestions(req) {
  const { quizId } = req.params;  // quizId validated by URI regex
  return listQuizQuestions(quizId)
    .then((questionList) => {
      const questions = questionList.map(
        // Only pass these three properties onto the client
        ({ id, question, answers }) => ({ id, question, answers }));
      return { questions };
    });
}


module.exports = { getAllQuizzes, getQuizQuestions };
