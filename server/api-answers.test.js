const httpMocks = require('node-mocks-http');
const apiAnswers = require('./api-answers');

// Note that, in the spirit of being no more complicicated than is necessary,
// these tests do not set up any special test data as the current approach
// used by the server of loading the data directly from a JSON file is already
// simple enough. So, all data verified in this file should match those in the
// 'data' directory.

const QUIZ_ONE_ID = '1';
const QUIZ_TWO_ID = '2';
const USER_ONE_ID = 'AA';
const USER_TWO_ID = 'BB';
const USER_THREE_ID = 'CC';

let response;
beforeEach(() => {
  response = httpMocks.createResponse();
});

const MIXTURE_OF_CORRECT_INCORRECT_ANSWERS = {
  '1a': '1a2', // correct
  '1b': '1b2', // incorrect
  '1c': '1c2', // correct
};

const ALL_CORRECT_ANSWERS = { '1a': '1a2', '1b': '1b1', '1c': '1c2' };

const ALL_INCORRECT_ANSWERS = { '1a': '1a1', '1b': '1b2', '1c': '1c3' };

describe('submitUserAnswers', () => {
  let request;
  describe('with a mixture of correct and incorrect answers', () => {
    beforeEach(() => {
      request = httpMocks.createRequest({
        params: { quizId: QUIZ_ONE_ID, userId: USER_ONE_ID },
        body: { answers: MIXTURE_OF_CORRECT_INCORRECT_ANSWERS }
      });
    });

    test('returns the correct set of correct answers', () => {
      return apiAnswers.submitUserAnswers(request, response)
        .then(({ correctAnswers }) => {
          expect(correctAnswers.sort()).toEqual(['1a', '1c']);
        });
    });

    test('includes the correct submission date', () => {
      // TODO(matt): Complete this test
    });
  });

  describe('with answers all correct', () => {
    beforeEach(() => {
      request = httpMocks.createRequest({
        params: { quizId: QUIZ_ONE_ID, userId: USER_ONE_ID },
        body: { answers: ALL_CORRECT_ANSWERS }
      });
    });

    test('returns the correct set of correct answers', () => {
      return apiAnswers.submitUserAnswers(request, response)
        .then(({ correctAnswers }) => {
          expect(correctAnswers.sort()).toEqual(['1a', '1b', '1c']);
        });
    });
  });

  describe('with answers all wrong', () => {
    beforeEach(() => {
      request = httpMocks.createRequest({
        params: { quizId: QUIZ_ONE_ID, userId: USER_ONE_ID },
        body: { answers: ALL_INCORRECT_ANSWERS }
      });
    });

    test('returns an empty set of correct answers', () => {
      return apiAnswers.submitUserAnswers(request, response)
        .then(({ correctAnswers }) => {
          expect(correctAnswers).toHaveLength(0);
        });
    });
  });

  describe('submitting a test a second time', () => {
    beforeEach(() => {
      const firstRequest = httpMocks.createRequest({
        params: { quizId: QUIZ_ONE_ID, userId: USER_ONE_ID },
        body: { answers: ALL_INCORRECT_ANSWERS }
      });
      const firstResponse = httpMocks.createResponse();
      request = httpMocks.createRequest({
        params: { quizId: QUIZ_ONE_ID },
        body: { answers: MIXTURE_OF_CORRECT_INCORRECT_ANSWERS }
      });
      return apiAnswers.submitUserAnswers(firstRequest, firstResponse);
    });

    test('returns the correct set of correct answers', () => {
      return apiAnswers.submitUserAnswers(request, response)
        .then(({ correctAnswers }) => {
          expect(correctAnswers.sort()).toEqual(['1a', '1c']);
        });
    });
  });

  describe('submitting a second test with a different user ID', () => {
    // TODO(matt): Add this test
  });

  describe('with an invalid quiz ID', () => {
    // TODO(matt): Add this test
  });

  describe('with an invalid question ID', () => {
    // TODO(matt): Add this test
  });

  describe('with an invalid answer ID', () => {
    // TODO(matt): Add this test
  });

  // TODO(matt): A test that reflects that the set of questions and/or answers
  // may have changed before submission
});

describe('getUserAnswers', () => {
  beforeEach(() => {
    const userOneRequest = httpMocks.createRequest({
      params: { quizId: QUIZ_ONE_ID, userId: USER_ONE_ID },
      body: { answers: MIXTURE_OF_CORRECT_INCORRECT_ANSWERS }
    });
    const userOneResponse = httpMocks.createResponse();
    const userTwoRequest = httpMocks.createRequest({
      params: { quizId: QUIZ_ONE_ID, userId: USER_TWO_ID },
      body: { answers: ALL_CORRECT_ANSWERS }
    });
    const userTwoResponse = httpMocks.createResponse();
    return apiAnswers.submitUserAnswers(userOneRequest, userOneResponse)
      .then(() => apiAnswers.submitUserAnswers(userTwoRequest, userTwoResponse));
  });

  it('returns the set of marked answers for the given user', () => {
    const getUserAnswersRequest = httpMocks.createRequest({
      params: { quizId: QUIZ_ONE_ID, userId: USER_ONE_ID },
    });
    return apiAnswers.getUserAnswers(getUserAnswersRequest, response)
      .then(({ correctAnswers }) => {
        expect(correctAnswers.sort()).toEqual(['1a', '1c']);
      });
  });

  it('returns a 404 response if the user has not submitted answers', () => {
    const getUserAnswersRequest = httpMocks.createRequest({
      params: { quizId: QUIZ_ONE_ID, userId: USER_THREE_ID },
    });
    return apiAnswers.getUserAnswers(getUserAnswersRequest, response)
      .then(() => {
        expect(response.statusCode).toEqual(404);
      });
  });

  it('returns a 404 response if the user has not submitted answers for this quiz', () => {
    const getUserAnswersRequest = httpMocks.createRequest({
      params: { quizId: QUIZ_TWO_ID, userId: USER_ONE_ID },
    });
    return apiAnswers.getUserAnswers(getUserAnswersRequest, response)
      .then(() => {
        expect(response.statusCode).toEqual(404);
      });
  });
});
