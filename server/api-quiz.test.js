const httpMocks = require('node-mocks-http');
const apiQuiz = require('./api-quiz');

// Note that, in the spirit of being no more complicicated than is necessary,
// these tests do not set up any special test data as the current approach
// used by the server of loading the data directly from a JSON file is already
// simple enough. So, all data verified in this file should match those in the
// 'data' directory.

const QUIZ_ONE_ID = '1';

let request;
let response;
beforeEach(() => {
  response = httpMocks.createResponse();
});

describe('getAllQuizzes with no user ID', () => {
  beforeEach(() => {
    request = httpMocks.createRequest();
  });

  test('returns the correct number of quizzes', () => {
    return apiQuiz.getAllQuizzes(request, response)
      .then(({ quizzes }) => {
        expect(quizzes).toHaveLength(4);
      });
  });

  test('each quiz has the correct order', () => {
    const expectedQuizOrders = [
      expect.objectContaining({id: '1', order: 1}),
      expect.objectContaining({id: '2', order: 3}),
      expect.objectContaining({id: '3', order: 2}),
    ];

    return apiQuiz.getAllQuizzes(request, response)
      .then(({ quizzes}) => {
        expect(quizzes).toEqual(expect.arrayContaining(expectedQuizOrders));
      });
  });
});

describe('getQuizQuestions with no user ID', () => {
  // TODO(matt): Add a test case for a second quiz - i.e. checking it doesn't
  // always return the data for quiz one.

  describe('for quiz one', () => {
    beforeEach(() => {
      request = httpMocks.createRequest({
        params: { quizId: QUIZ_ONE_ID },
      });
    });

    test('returns the correct number of questions', () => {
      return apiQuiz.getQuizQuestions(request, response)
        .then(({ questions }) => {
          expect(questions).toHaveLength(3);
        });
    });

    test('returns the expected question and answer IDs', () => {
      const expectedQuestions = [
        expect.objectContaining({ id: '1a', 'answers': [
          expect.objectContaining({ id: '1a1'}),
          expect.objectContaining({ id: '1a2'}),
        ]}),
        expect.objectContaining({ id: '1b', 'answers': [
          expect.objectContaining({ id: '1b1'}),
          expect.objectContaining({ id: '1b2'}),
        ]}),
        expect.objectContaining({ id: '1c', 'answers': [
          expect.objectContaining({ id: '1c1'}),
          expect.objectContaining({ id: '1c2'}),
          expect.objectContaining({ id: '1c3'}),
        ]})
      ];
      return apiQuiz.getQuizQuestions(request, response)
        .then(({ questions }) => {
          expect(questions).toEqual(expectedQuestions);
        });
    });

    test('only contains id, question and answers properties', () => {
      // in order to check the answer does not leak out
      const expectedKeys = ['id', 'question', 'answers'].sort();
      return apiQuiz.getQuizQuestions(request, response)
        .then(({ questions }) => {
          questions.forEach((question) => {
            expect(Object.keys(question).sort()).toEqual(expectedKeys);
          });
        });
    });
  });
});
