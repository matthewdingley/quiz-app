# README #

The Quiz-o-Matic

## Running The App ##
The app has a client side and server side component. The services for each of these will need to be running in order to run the app in development mode.

To start the server:
```
cd ./server
yarn start
```

And to start the web app (which is itself based on Create React App):
```
cd ./client
yarn start
```

Once both have been started, the app will be available at `http://localhost:3000`.