import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import Button from '@material/react-button';
import { QUIZ_PROP_TYPE, QUESTION_PROP_TYPE } from './propTypes';
import Question from './Question';


/**
 * Renders a quiz with questions and if available, answers marked.
 */
export default class Quiz extends Component {

  constructor(props) {
    super(props);
    this.state = { answers: {}, disabled: false };
    this.onAnswerChanged = this.onAnswerChanged.bind(this);
    this.onButtonPress = this.onButtonPress.bind(this);
  }

  onAnswerChanged(questionId, answerId) {
    const answers = { ...this.state.answers };
    answers[questionId] = answerId;
    this.setState({ answers });
  }

  onButtonPress() {
    const { onAnswersReady, quiz } = this.props;
    onAnswersReady(quiz.id, this.state.answers);
    this.setState({ disabled: true });
  }

  render() {
    const { quiz, questions, correctAnswers } = this.props;
    // TODO(matt): In order to show previous results, the currently selected
    // answer needs to be passed to each Question element as a property.
    const questionElements = questions.map((question) => {
      const isCorrect = correctAnswers !== undefined && correctAnswers.includes(question.id);
      return (
        <Question
          key={question.id}
          question={question}
          onChange={this.onAnswerChanged}
          isCorrect={isCorrect}
          showAnswer={correctAnswers !== undefined}
          disabled={this.state.disabled} />
      );
    });
    return (
      <div className="Quiz">
        <h2>{quiz.name}</h2>
        <Link to="/">Back to list</Link>
        {questionElements}
        <Button
          raised
          onClick={this.onButtonPress}
          disabled={this.state.disabled}
        >Mark my answers!</Button>
      </div>
    );
  }
}
Quiz.propTypes = {
  quiz: QUIZ_PROP_TYPE.isRequired,
  questions: PropTypes.arrayOf(QUESTION_PROP_TYPE).isRequired,
  correctAnswers: PropTypes.arrayOf(PropTypes.string),
  onAnswersReady: PropTypes.func.isRequired,
}