import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import Button from '@material/react-button';
import Card, {
  CardPrimaryContent,
  CardActions,
  CardActionButtons
} from '@material/react-card';
import { QUIZ_PROP_TYPE } from './propTypes';
import './QuizList.scss';

function orderSortFn(a, b) {
  return a.order - b.order;
}

function QuizTile(props) {
  const { id, name, color } = props.quiz;
  const bannerStyle = { backgroundColor: color };
  // TODO(matt): This link style should not be necessary
  const linkStyle = { textDecoration: 'none' };
  const quizPath = `/quiz/${id}`;
  return (
    <div className="QuizTile">
      <Link to={quizPath} style={linkStyle}>
        <Card>
          <CardPrimaryContent>
            <div className="QuizTileBanner" style={bannerStyle}>
              <h2>{name}</h2>
            </div>
          </CardPrimaryContent>
          <CardActions>
            <CardActionButtons>
              <Button>Take Quiz</Button>
            </CardActionButtons>
         
          </CardActions>
        </Card>
      </Link>
    </div>
  );
}
QuizTile.propTypes = {
  quiz: QUIZ_PROP_TYPE.isRequired,
};

/**
 * A simple component to render a given list of quizzes.
 */
export default function QuizList(props) {
  const { quizzes } = props;
  const sortedQuizzes = quizzes.sort(orderSortFn);
  const quizElements = sortedQuizzes.map((quiz) => {
    return <QuizTile key={quiz.id} quiz={quiz} />
  });
  return (
    <div className="QuizList">
      {quizElements}
    </div>
  );
}
QuizList.propTypes = {
  quizzes: PropTypes.arrayOf(QUIZ_PROP_TYPE).isRequired
};