import React from 'react';
import Radio, {NativeRadioControl} from '@material/react-radio';
import PropTypes from 'prop-types';
import { QUESTION_PROP_TYPE } from './propTypes';
import './Question.scss';

function QuizCorrectAnswer(props) {
  return <p className="QuizCorrectAnswer">Correct answer!</p>
}

function QuizWrongAnswer(props) {
  return <p className="QuizWrongAnswer">Wrong answer</p>
}

export default function Question(props) {
  const { showAnswer, isCorrect, disabled, onChange } = props;
  // TODO(matt): The clash between the question property and the name for the
  // property holding the text of the question itself is causing problems.
  // This needs refactoring.
  const { question, answers } = props.question;
  const radioOptions = answers.map(({ id, answer }) => (
    <div className="QuestionAnswer" key={id}>
      <Radio label={answer}>
        <NativeRadioControl
          name={props.question.id}
          value={answer}
          id={id}
          onChange={(e) => onChange(props.question.id, e.target.id)}
          disabled={disabled}
        />
      </Radio>
    </div>
  ));
  let answer = null;
  if (showAnswer) {
    answer = (isCorrect) ? <QuizCorrectAnswer /> : <QuizWrongAnswer />;

  }
  return (
    <div className="Question">
      <h3>{question}</h3>
      {answer}
      {radioOptions}
    </div>
  );
}
Question.propTypes = {
  question: QUESTION_PROP_TYPE.isRequired,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  isCorrect: PropTypes.bool,
  showAnswer: PropTypes.bool,
};
Question.defaultValues = {
  disabled: false,
  showAnswer: false
};