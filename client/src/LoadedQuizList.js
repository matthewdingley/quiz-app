import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import LoadingList from './LoadingList';
import LoadedQuiz from './LoadedQuiz';
import QuizList from './QuizList';

function getAllQuizzes() {
  return fetch('/api/quizzes/')
    .then(response => response.json())
    .then(({ quizzes }) => quizzes);
}

function LoadingFailed(props) {
  // TODO(matt): Display a nice message with a button to try again. Have the button emit a relevant event.
  return (<div>Loading failed</div>);
}

export default class LoadedQuizList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quizzes: null,
      quizLoadingFailed: false,
    };
  }

  componentDidMount() {
    getAllQuizzes()
      .then(quizzes => this.setState({ quizzes }))
      // TODO(matt): Consider allowing the error to be logged somewhere.
      .catch(() => this.setState({ quizLoadingFailed: true }));
  }

  render() {
    const { quizzes, quizLoadingFailed } = this.state;
    if (quizLoadingFailed) {
      return <LoadingFailed />;
    } else if (quizzes === null) {
      return <LoadingList />;
    }
    // TODO(matt): It'd be best to use a centralised store of data as it arrives
    // e.g. using Redux. This would allow the list of quizzes to be stored
    // centrally and then obtained independently by the quiz list and the
    // LoadedQuiz (which needs the list of quizzes to get the quiz name).
    return (
      <Router>
        <div className="LoadedQuizList">
          <Route
            path="/"
            exact
            render={props => <QuizList {...props} quizzes={quizzes} />}
          />
          <Route
            path="/quiz/:quizId"
            render={props => <LoadedQuiz {...props} quizzes={quizzes} />}
          />
        </div>
      </Router>
    );
  }  
}
