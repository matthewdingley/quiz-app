import PropTypes from 'prop-types';

export const QUIZ_PROP_TYPE = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  order: PropTypes.number.isRequired,
});

export const QUESTION_PROP_TYPE = PropTypes.shape({
  id: PropTypes.string.isRequired,
  question: PropTypes.string.isRequired,
  answers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    answer: PropTypes.string.isRequired
  })).isRequired
});
