import React from 'react';

/**
 * Displays a view of empty list items while loading.
 */
export default function LoadingList() {
  // TODO(matt): Display a loading view
  return <div>Loading list</div>;
}
