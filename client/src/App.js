import React, { Component } from 'react';
import LoadedQuizList from './LoadedQuizList';
import './App.scss';

class App extends Component {

  render() {
    return (
      <div className="App">
        <h1>Quiz-o-Matic</h1>
        <LoadedQuizList />
      </div>
    );
  }
}

export default App;
