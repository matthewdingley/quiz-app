import React, { Component } from 'react';
import LoadingQuestions from './LoadingQuestions';
import Quiz from './Quiz';

// TODO(matt): Set up user accounts!
const USER_ID = 'userOne';

function getQuizQuestions(quizId) {
  return fetch(`/api/quiz/${quizId}/questions`)
    .then(response => response.json())
    .then(({ questions }) => questions);
}

function LoadingFailed(props) {
  // TODO(matt): Display a nice message with a button to try again. Have the button emit a relevant event.
  return (<div>Loading failed</div>);
}

export default class LoadedQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: null,
      quizLoadingFailed: false,
      correctAnswers: undefined,
    };
    this.onAnswersReady =this.onAnswersReady.bind(this);
  }

  componentDidMount() {
    const { quizId } = this.props.match.params;
    getQuizQuestions(quizId)
      .then(questions => this.setState({ questions }))
      // TODO(matt): Consider allowing the error to be logged somewhere.
      .catch(() => this.setState({ quizLoadingFailed: true }));
  }

  onAnswersReady(quizId, answers) {
    const postUri = `/api/user/${USER_ID}/answers/${quizId}`;
    fetch(postUri, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify({ answers })
    })
      .then(response => response.json())
      .then(({ correctAnswers }) => this.setState({ correctAnswers }));
  }

  render() {
    const { questions, quizLoadingFailed } = this.state;
    if (quizLoadingFailed) {
      return <LoadingFailed />;
    } else if (questions === null) {
      return <LoadingQuestions />;
    }
    const { quizId } = this.props.match.params;
    const { quizzes } = this.props;
    const quiz = quizzes.find(quiz => quiz.id === quizId);
    if (quiz === undefined) {
      throw new Error('Unexpected quiz ID');
    }
    return (
      <div className="LoadedQuiz">
        <Quiz
          quiz={quiz}
          questions={questions}
          onAnswersReady={this.onAnswersReady}
          correctAnswers={this.state.correctAnswers}
          />
      </div>
    );
  }  
}
